//! # food_recipes
//!
#![warn(missing_debug_implementations, rust_2018_idioms, missing_docs)]

#[cfg(test)]
extern crate quickcheck;
#[cfg(test)]
#[macro_use(quickcheck)]
extern crate quickcheck_macros;

pub mod recipes {
    //! recipes
    //!
    //! The recipes modules contains the main structures used to build recipes applications.

    /// Structure that will contain details of an ingredient.
    #[derive(Debug, Clone)]
    pub struct Ingredient {
        /// Ingredient name
        pub name: String,
        /// Unit of measure
        pub unit_of_measure: String,
    }

    /// Structure that will contain information regarding an ingredient in a recipe.
    #[derive(Debug, Clone)]
    pub struct RecipeIngredient {
        /// Ingredient instance
        pub ingredient: Ingredient,
        /// Quantity
        pub quantity: f64,
        /// If you don't have the ingredient, you could use this as an alternative.
        pub alternative: Option<Box<RecipeIngredient>>,
    }

    /// Struct that will contain a recipe
    #[derive(Debug, Clone)]
    pub struct Recipe {
        name: String,
        summary: String,
        ingredients: std::vec::Vec<RecipeIngredient>,
        steps: std::vec::Vec<String>,
    }

    impl Recipe {
        /// Example
        /// ```rust
        /// use food_recipes::recipes::Recipe;
        ///
        /// let recipe = Recipe::new("My awesome recipe");
        ///
        /// assert_eq!("My awesome recipe", recipe.get_name());
        /// ```
        ///
        /// Recipe name can't be empty. The default recipe name is "Recipe".
        /// ```rust
        /// use food_recipes::recipes::Recipe;
        ///
        /// let recipe = Recipe::new("");
        ///
        /// assert_eq!("Recipe", recipe.get_name());
        /// ```
        pub fn new(name: &str) -> Recipe {
            let recipe_name = if name != "" { name } else { "Recipe" };
            Recipe {
                name: String::from(recipe_name),
                summary: String::from(""),
                ingredients: vec![],
                steps: vec![],
            }
        }

        /// Get the name of the recipe.
        ///
        /// ```rust
        /// use food_recipes::recipes::Recipe;
        ///
        /// let recipe = Recipe::new("Recipe name");
        ///
        /// assert_eq!("Recipe name", recipe.get_name());
        /// ```
        pub fn get_name(&self) -> String {
            self.name.clone()
        }

        /// Change the name of the recipe.
        ///
        /// ```rust
        /// use food_recipes::recipes::Recipe;
        ///
        /// let mut recipe = Recipe::new("Recipe name");
        /// recipe.set_name("New recipe name");
        ///
        /// assert_eq!("New recipe name", recipe.get_name());
        /// ```
        pub fn set_name(&mut self, name: &str) {
            if name != "" {
                self.name = String::from(name);
            }
        }

        /// Get the short summary of the recipe.
        /// Default value is empty string.
        ///
        /// ```rust
        /// use food_recipes::recipes::Recipe;
        ///
        /// let recipe = Recipe::new("Recipe name");
        ///
        /// assert_eq!("", recipe.get_summary());
        /// ```
        pub fn get_summary(&self) -> String {
            self.summary.clone()
        }

        /// Change the summary
        ///
        /// ```rust
        /// use food_recipes::recipes::Recipe;
        ///
        /// let mut recipe = Recipe::new("Recipe name");
        /// recipe.set_summary("New recipe summary");
        ///
        /// assert_eq!("New recipe summary", recipe.get_summary());
        /// ```
        pub fn set_summary(&mut self, summary: &str) {
            self.summary = String::from(summary);
        }

        /// Add an ingredient to the recipe.
        ///
        /// ```rust
        /// use food_recipes::recipes::Recipe;
        ///
        /// let mut tea = Recipe::new("Recipe name");
        ///
        /// tea.add_ingredient("Water", 0.5, "l");
        /// tea.add_ingredient("Tea", 1.0, "bag");
        ///
        /// assert_eq!(2, tea.get_ingredients().len());
        /// ```
        pub fn add_ingredient(&mut self, name: &str, quantity: f64, unit_of_measure: &str) {
            self.ingredients.push(RecipeIngredient {
                ingredient: Ingredient {
                    name: String::from(name),
                    unit_of_measure: String::from(unit_of_measure),
                },
                quantity,
                alternative: None,
            });
        }

        /// Returns a vector of the recipe ingredients.
        ///
        /// Note: I reaylly don't like this test, it depends on to many inside details. I would
        /// prefer to test a json or a displayable list.
        ///
        /// ```rust
        /// use food_recipes::recipes::{ Recipe, RecipeIngredient, Ingredient };
        ///
        /// let mut recipe = Recipe::new("Recipe name");
        ///
        /// recipe.add_ingredient("Ingredient", 1.0, "unit of measure");
        ///
        /// let mut ingredients = recipe.get_ingredients().iter();
        /// let ingredient = &ingredients.next().unwrap().ingredient;
        /// assert_eq!("Ingredient", ingredient.name);
        /// assert_eq!("unit of measure", ingredient.unit_of_measure);
        /// ```
        pub fn get_ingredients(&self) -> &std::vec::Vec<RecipeIngredient> {
            &self.ingredients
        }

        /// Adds a step to a recipe
        ///
        /// ```rust
        /// use food_recipes::recipes::Recipe;
        ///
        /// let mut recipe = Recipe::new("Recipe name");
        ///
        /// recipe.add_step("Step 1");
        /// recipe.add_step("Step 2");
        ///
        /// let mut steps = recipe.get_steps().iter();
        /// assert_eq!("Step 1", steps.next().unwrap());
        /// assert_eq!("Step 2", steps.next().unwrap());
        pub fn add_step(&mut self, step: &str) {
            &self.steps.push(String::from(step));
        }

        /// Returns a vector of the recipe steps.
        pub fn get_steps(&self) -> &std::vec::Vec<String> {
            &self.steps
        }
    }
}

#[cfg(test)]
mod recipe_tests {
    use quickcheck::TestResult;

    use super::recipes::*;

    #[test]
    fn default_name() {
        let recipe = Recipe::new("");

        assert_eq!("Recipe", recipe.get_name());
    }

    #[quickcheck]
    fn recipe_has_name(recipe_name: String) -> TestResult {
        if recipe_name.is_empty() {
            return TestResult::discard();
        }

        let recipe = Recipe::new(&recipe_name);

        TestResult::from_bool(recipe_name == recipe.get_name())
    }

    #[quickcheck]
    fn recipe_name_can_change(first_name: String, second_name: String) -> TestResult {
        if first_name.is_empty() || second_name.is_empty() {
            return TestResult::discard();
        }

        let mut recipe = Recipe::new(&first_name);

        recipe.set_name(&second_name);

        TestResult::from_bool(second_name == recipe.get_name())
    }

    #[quickcheck]
    fn recipe_name_can_not_be_empty(recipe_name: String) -> TestResult {
        if recipe_name.is_empty() {
            return TestResult::discard();
        }

        let mut recipe = Recipe::new(&recipe_name);

        recipe.set_name("");

        TestResult::from_bool(recipe_name == recipe.get_name())
    }

    #[quickcheck]
    fn recipe_has_an_empty_summary_as_default(recipe_name: String) {
        let recipe = Recipe::new(&recipe_name);

        assert_eq!("", recipe.get_summary());
    }

    #[quickcheck]
    fn recipe_summary_can_change(recipe_name: String, recipe_summary: String) {
        let mut recipe = Recipe::new(&recipe_name);

        recipe.set_summary(&recipe_summary);

        assert_eq!(recipe_summary, recipe.get_summary());
    }

    #[test]
    fn recipe_starts_with_no_ingredients() {
        let recipe = Recipe::new("Recipe");

        assert!(recipe.get_ingredients().is_empty());
    }

    #[quickcheck]
    fn you_can_add_ingredients_to_a_recipe(
        ingredient_name: String,
        quantity: f64,
        unit_of_measure: String,
    ) -> TestResult {
        if ingredient_name.is_empty() || unit_of_measure.is_empty() {
            return TestResult::discard();
        }

        let mut recipe = Recipe::new("Recipe");

        recipe.add_ingredient(&ingredient_name, quantity, &unit_of_measure);

        TestResult::from_bool(recipe.get_ingredients().len() == 1)
    }

    #[test]
    fn you_can_list_recipe_ingredients() {
        let mut recipe = Recipe::new("Recipe");

        recipe.add_ingredient("Flour", 1.0, "cups");
        recipe.add_ingredient("Eggs", 0.4, "dozen");

        let ingredients = recipe.get_ingredients();

        assert_eq!("Flour", ingredients.first().unwrap().ingredient.name);
        assert_eq!("Eggs", ingredients[1].ingredient.name);
    }

    #[quickcheck]
    fn you_can_add_steps_to_recipe(step: String) -> TestResult {
        if step.is_empty() {
            return TestResult::discard();
        }

        let mut recipe = Recipe::new("Recipe");

        recipe.add_step("Step 1");

        TestResult::from_bool(recipe.get_steps().len() == 1)
    }

    #[test]
    fn you_can_enumerate_steps() {
        let mut recipe = Recipe::new("Recipe");

        recipe.add_step("Step 1");
        recipe.add_step("Step 2");

        let mut steps = recipe.get_steps().iter();
        assert_eq!("Step 1", steps.next().unwrap());
        assert_eq!("Step 2", steps.next().unwrap());
    }
}
